<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\Internship;
use App\Entity\Event;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i<10; $i++){
         $internship = new Internship();
         $internship->setStatus("En cours");
         $internship->setcompanyName("Fausse entreprise n°".$i);
         $internship->setwebsite("Faux site internet n°".$i);
         $internship->setobservations("Lorem ipsum dolor sit, amet consectetur adipisicing elit. A consequuntur, quae excepturi quas eum officia praesentium autem dicta ipsum saepe. ");
         $internship->setemail("Faux email n°".$i);
         $internship->setphone("Faux numéro de tel n°".$i);
         $internship->setjobAdUrl("http://indeed.com/fakead");
         $internship->seteventDate(new \DateTime('now'));
         $internship->setanswer("pas encore");
         $internship->setinterest(mt_rand(0, 5));
         
         $event = new Event();
         for ($j=0; $j<3; $j++){
         $event->seteventType("Type d'évennement ".$j);
         $event->seteventDescription("Description d'évennement ".$j);
         $manager->persist($event);
         $event->setInternship($internship);
         $manager->persist($internship);
         }
         

         
        
        }
        

        $manager->flush();
    }
}
