<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


use App\Entity\Internship;
use App\Entity\User;
use App\Form\InternshipType;

class InternshipController extends AbstractController
{
   
    /**
     * @Route({
     *     "en": "/internship",
     *     "fr": "/stage"
     * }, name="internship")
     */
    public function index()
    {
        return $this->render('internship/index.html.twig', [
            'controller_name' => 'InternshipController',
        ]);
    }

     /**
     * @Route({
     *     "en": "/internship/show_all",
     *     "fr": "/stage/afficher_tous"
     * }, name="internship_show_all")
     */
    public function showAll()
    {
        $repo = $this->getDoctrine()->getRepository(Internship::class);
        // $internships = $repo->findAll();
        $userID = $this->getUser();
        $internships = $repo->findBy(
            ['user' => $userID]
        );
        $userID = $this->getUser();
        return $this->render('internship/showAll.html.twig', [
            'controller_name' => 'InternshipController',
            'internships' => $internships,
            'userID' => $userID,
        ]);
    }

      /**
     * @Route({
     *     "en": "/internship/add",
     *     "fr": "/stage/ajouter"
     * }, name="internship_add")
     */
    public function new(Request $request)
    {
        
        $internship = new Internship();
        $form = $this->createForm(InternshipType::class, $internship);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $internship->setdeleted(false);
            $userID = $this->getUser();
            $internship->setuser($userID);
            $task = $form->getData();
            
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('internship_show_all');
        }

        return $this->render('internship/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

     /**
     * @Route({
     *     "en": "/internship/view/{id}",
     *     "fr": "/stage/afficher/{id}"
     * }, name="internship_view")
     */
    public function view($id)
    {
        $repo = $this->getDoctrine()->getRepository(Internship::class);
        $internship = $repo->find($id);
        return $this->render('internship/view.html.twig', [
            'controller_name' => 'InternshipController',
            'internship' => $internship
        ]);
    }

     /**
     * @Route({
     *     "en": "/internship/delete/{id}",
     *     "fr": "/stage/supprimer/{id}"
     * }, name="internship_delete")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $internship = $entityManager->getRepository(Internship::class)->find($id);
        $internship->setDeleted(true);
        $entityManager->flush();
        $this->addFlash(
            'notice',
            'Your changes were saved!'
        );
        return $this->redirectToRoute('internship_show_all');
        
    }

    /**
     * @Route({
     *     "en": "/internship/edit/{id}",
     *     "fr": "/stage/modifier/{id}"
     * }, name="internship_edit")
     */
    public function edit(Internship $internship, Request $request)
    {
        $form = $this->createForm(InternshipType::class, $internship);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();
            return $this->redirectToRoute('internship_show_all');
        }

        return $this->render('internship/edit.html.twig', [
            'internship' => $internship,
            'form' => $form->createView()
        ]);
    }

}
