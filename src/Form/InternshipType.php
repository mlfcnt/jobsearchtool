<?php

namespace App\Form;

use App\Entity\Internship;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Event;





class InternshipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status')
            ->add('jobTitle')
            ->add('companyName')
            ->add('website')
            ->add('observations')
            ->add('email')
            ->add('phone')
            ->add('jobAdUrl')
            ->add('eventDate', DateType::class, [
                'widget' => 'choice',
            ])
            ->add('lastEvent')
            ->add('nextStep')
            ->add('interest', null, [
            'help' => 'Une note entre 0 et 5. 5 étant tu es super motivé pour l\'avoir, 0 pas des masses.',
            ])
            ->add('answer')
            ->add('save', SubmitType::class, ['label' => 'Ajouter']);
            }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Internship::class,
        ]);
    }
}
